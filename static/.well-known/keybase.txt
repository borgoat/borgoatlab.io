==================================================================
https://keybase.io/giorgioazzinnaro
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of http://giorgio.azzinna.ro
  * I am giorgioazzinnaro (https://keybase.io/giorgioazzinnaro) on keybase.
  * I have a public key ASDTDJVXz9dApLrhclKTFWWvr4d_BTNyraYzpPNsEDj1zwo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120d30c9557cfd740a4bae17252931565afaf877f053372ada633a4f36c1038f5cf0a",
      "host": "keybase.io",
      "kid": "0120d30c9557cfd740a4bae17252931565afaf877f053372ada633a4f36c1038f5cf0a",
      "uid": "345447587ba2b913076f14695f6dfe19",
      "username": "giorgioazzinnaro"
    },
    "merkle_root": {
      "ctime": 1506150502,
      "hash": "d02b8c4152e6eba0ee27280a8bd3d6e7a3f7741d7d7436f5349824f5aefa027651a9c140754764f53b11a01d4684299b5a51dbf0fb76386318dd5d0c4ea0be12",
      "hash_meta": "7c6c06f8fec958997ad5c538975b33d59af2973446467434cc5a86f216f25dec",
      "seqno": 1447381
    },
    "service": {
      "hostname": "giorgio.azzinna.ro",
      "protocol": "http:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.30"
  },
  "ctime": 1506150609,
  "expire_in": 504576000,
  "prev": "4a14162cf4c7e480ca54925184c4f548225ef8cd4565f7f342782e1fab2131bb",
  "seqno": 13,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg0wyVV8/XQKS64XJSkxVlr6+HfwUzcq2mM6TzbBA49c8Kp3BheWxvYWTFA1J7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwZDMwYzk1NTdjZmQ3NDBhNGJhZTE3MjUyOTMxNTY1YWZhZjg3N2YwNTMzNzJhZGE2MzNhNGYzNmMxMDM4ZjVjZjBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwZDMwYzk1NTdjZmQ3NDBhNGJhZTE3MjUyOTMxNTY1YWZhZjg3N2YwNTMzNzJhZGE2MzNhNGYzNmMxMDM4ZjVjZjBhIiwidWlkIjoiMzQ1NDQ3NTg3YmEyYjkxMzA3NmYxNDY5NWY2ZGZlMTkiLCJ1c2VybmFtZSI6Imdpb3JnaW9henppbm5hcm8ifSwibWVya2xlX3Jvb3QiOnsiY3RpbWUiOjE1MDYxNTA1MDIsImhhc2giOiJkMDJiOGM0MTUyZTZlYmEwZWUyNzI4MGE4YmQzZDZlN2EzZjc3NDFkN2Q3NDM2ZjUzNDk4MjRmNWFlZmEwMjc2NTFhOWMxNDA3NTQ3NjRmNTNiMTFhMDFkNDY4NDI5OWI1YTUxZGJmMGZiNzYzODYzMThkZDVkMGM0ZWEwYmUxMiIsImhhc2hfbWV0YSI6IjdjNmMwNmY4ZmVjOTU4OTk3YWQ1YzUzODk3NWIzM2Q1OWFmMjk3MzQ0NjQ2NzQzNGNjNWE4NmYyMTZmMjVkZWMiLCJzZXFubyI6MTQ0NzM4MX0sInNlcnZpY2UiOnsiaG9zdG5hbWUiOiJnaW9yZ2lvLmF6emlubmEucm8iLCJwcm90b2NvbCI6Imh0dHA6In0sInR5cGUiOiJ3ZWJfc2VydmljZV9iaW5kaW5nIiwidmVyc2lvbiI6MX0sImNsaWVudCI6eyJuYW1lIjoia2V5YmFzZS5pbyBnbyBjbGllbnQiLCJ2ZXJzaW9uIjoiMS4wLjMwIn0sImN0aW1lIjoxNTA2MTUwNjA5LCJleHBpcmVfaW4iOjUwNDU3NjAwMCwicHJldiI6IjRhMTQxNjJjZjRjN2U0ODBjYTU0OTI1MTg0YzRmNTQ4MjI1ZWY4Y2Q0NTY1ZjdmMzQyNzgyZTFmYWIyMTMxYmIiLCJzZXFubyI6MTMsInRhZyI6InNpZ25hdHVyZSJ9o3NpZ8RA3K2SWHayvVlt6a5HQBKGYaA+cRjIHbEVYKRBkOcwWDK/6Qju3xRO8Qv/AESLYtsRtHilrNEYwCZ1ij00Bo/xBKhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEINm2LJ8HeVTlj/LSEii20ofjWniUB3CCNs/e7TO9J/3/o3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/giorgioazzinnaro

==================================================================

